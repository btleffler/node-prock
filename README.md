# Prock - _No Fuss Child Processes_

Prock lets you spawn child processes with the ability to listen for standard output, and affords the simplicity of passing a callback like you would with [`child_process.exec`](https://nodejs.org/dist/latest/docs/api/child_process.html#child_process_child_process_exec_command_options_callback).

```js
const Prock = require("prock");

Prock.init("git status", {
  "cwd": pathToGitRepo
}, (err, stdoutString, stderrString, exitCode, exitSignal) => {
  if (err) {
    return handleError(err);
  }

  // ...
}).on("output", (stdoutputString) => {
  // Data is either from stdout or stderr of the child process
}).on("out", (stdoutString) => {
  // Do something with the output
}).on("err", (stderrString) => {
  // Do something with the error output
});
```

Or use it like any other class:

```js
const Prock = require("prock");

let Process = new Prock(someCommand, someOptions, someCallback);
```

### Prock instances try to be a drop in replacement for normal [`ChildProcess`](https://nodejs.org/dist/latest/docs/api/child_process.html#child_process_class_childprocess) objects

They offer all the same event subscriptions, prototype methods, and properties as Node's `ChildProcess` objects. The only exception is that an `error` event handler isn't required if a callback is passed to the constructor. In that case, the error will be saved, and passed to the callback. Of course, if you want to subscribe to the `error` event, you're welcome to.

### Use it like `child_process.exec`

```js
const Prock = require("prock");

Prock.exec("cat someFile.txt", {
  "env": {
    "ENV_VAR": "ENV_VAL" // Completely uneeded in this case, but whatever
  }
}, (err, stdout, stderr, exitCode, exitSignal) => {
  // stdout = the contents of someFile.txt
});
```

### Use it like `child_process.spawn`

```js
const spawn = require("prock").spawn;
const ls = Prock.spawn("ls", [ "-lh", "/usr" ]);

ls.on("out", (data) => {
  console.log(`stdout: ${data}`);
}).on("err", (data) => {
  console.log(`stderr: ${data}`);
}).on("close", (code) => {
  console.log(`child process exited with code ${code}`);
});
```

## Notes / Limitations

Although Prock lets you pass a string as the command unlike `child_process.spawn`, it doesn't let you easily redirect IO like you can using `child_process.exec`.

```js
const Prock = require("prock");

// This won't work
let Proc = new Prock("echo $SOMETHING > out.txt", (error, out, err) => {
	// Pretty much nothing happend, same as trying to do:
	// child_proces.spawn("echo", [ "$SOMETHING", '>', "out.txt" ]);
});
```

## Todo

- [ ] Feature requests welcome
- [ ] Handle IO redirection [(`>`, `>>`, etc.)](http://www.tldp.org/LDP/abs/html/io-redirection.html) - _Create multiple `Prock` instances, and pipe stdio around._

[MIT License - Copyright © 2016 Benjamin Leffler](LICENSE)
