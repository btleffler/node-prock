# 0.1.3
 - Add ability to use `timeout` option like other Child_Process.exec type functions

# 0.1.2
 - Fix [Can't pass callback without options](https://gitlab.com/btleffler/node-prock/issues/1).

# 0.1.1
 - Initial Release
