"use strict";

const ChildProcess = require("child_process");
const EventEmitter = require("events");
const Os = require("os");
const Util = require("util");

/**
 * Get key/value pairs from an object (options)
 * @param {Object} opts The options object
 * @yield {Object}      Object containing the `key` and `value` properties
 */
function* optEntries (opts) {
	for (let key of Object.keys(opts)) {
		yield { "key": key, "value": opts[key] };
	}
}

/**
 * Split the command string into the command and its arguments
 * @param  {String} command The command string
 * @return {Object}         Object containing the `command` and `arguments`
 *                          properties
 */
function processCommand (command) {
	command = command.split(/\s/);

	return {
		"command": command.shift(),
		"arguments": command
	};
}

/**
 * Convert a function's arguments to an array
 * @method argumentsToArray
 * @param  {Arguments} args A function's arguments
 * @return {Array}          The function's arguments as an array
 */
function argumentsToArray (args) {
	return args.length === 1 ? [ arguments[0] ] : Array.apply(null, args);
}

/**
 * Prock - No Fuss Child Processes
 * @type  {Prock}
 * @param {String}   command  Command to run
 * @param {Object}   options  Optional options to pass. See ChildProcess.spawn
 *                            options
 * @param {Function} callback Optional callback to be executed after the command
 *                            is completed.
 *                            (err, stdOut, stdErr, code, signal) => { ... }
 *                            NOTE: code and signal are unavailable if an error
 *                            occurred
 */
var Prock = class Prock {
	constructor (command/*, options, callback*/) {
		let self = this;
		let options = {};
		let opts = typeof arguments[1] === "object" ? arguments[1] : {};
		let callback = () => {};
		let callbackConfigured = false;
		let cmd = processCommand(command);
		let terminated = false;
		let stdioClosed = false;
		let callbackExecuted = false;
		let errorCallbackSubscribed = false;
		let callbackArgs = [ null, "", "", null, null ];
		let proc, killTimer;

		if (arguments[1] instanceof Function) {
			callback = arguments[1];
			callbackConfigured = true;
		} else if (arguments[2] instanceof Function) {
			callback = arguments[2];
			callbackConfigured = true;
		}

		EventEmitter.call(this);

		this.on("newListener", (event) => {
			if (event === "error") {
				errorCallbackSubscribed = true;
			}
		});

		for (let entry of optEntries(opts)) {
			if (entry.key === "stdio") {
				continue;
			}

			options[entry.key] = entry.value;
		}

		options.killSignal = options.killSignal || 'SIGTERM';
		this.options = options;

		function executeCallback () {
			if (terminated && stdioClosed && !callbackExecuted) {
				callbackExecuted = true;
				callback.apply(self, callbackArgs);
			}
		}

		proc = ChildProcess.spawn(cmd.command, cmd.arguments, options);

		if (options.hasOwnProperty('timeout') && options.timeout > 0) {
			killTimer = setTimeout(() => {
				proc.kill(options.killSignal);
			}, options.timeout);
		}

		Object.defineProperty(this, "process", {
			"value": proc,
			"enumerable": true
		});

		//
		// Convenience events for std output
		//   ProckInstance.on("out", (<String>stdoutData) => {
		//     ...
		//   }).on("err", (<String>stderrData) => {
		//     ...
		//   }).on("output", (<String>anyData) => {
		//     ...
		//   })
		//
		proc.stdout.on("data", (chunk) => {
			chunk = chunk.toString();
			callbackArgs[1] += chunk;

			self.emit("out", chunk);
			self.emit("output", chunk);
		});

		proc.stderr.on("data", (chunk) => {
			chunk = chunk.toString();
			callbackArgs[2] += chunk;

			self.emit("err", chunk);
			self.emit("output", chunk);
		});

		//
		// Pass the ChildProcess' events to the Prock Instance
		//
		proc.on("close", function prockChildClose () {
			let args = argumentsToArray(arguments);

			clearTimeout(killTimer);
			EventEmitter.prototype.emit.apply(self, [ "close" ].concat(args));

			stdioClosed = true;
			executeCallback();
		}).on("disconnect", function prockChildDisconnect () {
			let args = argumentsToArray(arguments);

			EventEmitter.prototype.emit.apply(self, [ "disconnect" ].concat(args));
		}).on("error", function prockChildError (error) {
			let args = argumentsToArray(arguments);

			clearTimeout(killTimer);

			// Only emit errors if there was no callback configured,
			// otherwise it will be passed to the user's callback
			if (!callbackConfigured || errorCallbackSubscribed) {
				EventEmitter.prototype.emit.apply(self, [ "error" ].concat(args));
			}

			callbackArgs[0] = error;
			terminated = true;
		}).on("exit", function prockChildExit (code, signal) {
			let args = argumentsToArray(arguments);

			clearTimeout(killTimer);
			EventEmitter.prototype.emit.apply(self, [ "exit" ].concat(args));

			callbackArgs[3] = code;
			callbackArgs[4] = signal;
			terminated = true;
			executeCallback();
		}).on("message", function prockChildMessage () {
			let args = argumentsToArray(arguments);

			EventEmitter.prototype.emit.apply(self, [ "message" ].concat(args));
		});

		return this;
	}

	/**
	 * Check whether it's still possible to send and receive messages from the
	 * process
	 * @property connected
	 * @return   {Boolean}  True until Prock.disconnect() is called
	 */
	get connected () {
		return this.process.connected;
	}

	/**
	 * Close the IPC channel between parent and child
	 * @method disconnect
	 */
	disconnect () {
		return this.process.disconnect();
	}

	/**
	 * Send a signal to the child
	 * @method kill
	 * @param  {String} signal Optional. Signal to send to the child. Defaults to
	 *                         SIGTERM
	 */
	kill (/*signal*/) {
		return this.process.kill(
			arguments[0] || this.options.killSignal
		);
	}

	/**
	 * The process identifier of the child
	 * @property pid
	 * @return   {Number} The process identifier of the child
	 */
	get pid () {
		return this.process.pid;
	}

	/**
	 * Send a message to the child
	 * @method send
	 * @param  {Object}   message    The JSON.serialize()-able object to send to
	 *                               the child
	 * @param  {Handle}   sendHandle Optional. A TCP Server or Socket Object to be
	 *                               passed to the child
	 * @param  {Object}   options    Optional. Parameterize the sending of certain
	 *                               types of handles. Accepts {Boolean }`keepOpen`
	 * @param  {Function} callback   Optional. A callback to be called after the
	 *                               message has been sent to the child, but
	 *                               before it may have received it
	 * @return {Boolean}             True unless the IPC channel to the child has
	 *                               been closed, or the backlog of unset messages
	 *                               exeeds a threshold that makes it unwise to
	 *                               send more.
	 */
	send (message/*, sendHandle, options, callback*/) {
		let proc = this.process;
		let args = argumentsToArray(arguments);

		return ChildProcess.prototype.send.apply(proc, args);
	}

	/**
	 * A readable stream that represents the child's stderr
	 * @property stderr
	 * @return   {ReadableStream} A readable stream that represents the child's
	 *                            stderr
	 */
	get stderr () {
		return this.process.stderr;
	}

	/**
	 * A writable stream that represents the child's stdin
	 * @property stdin
	 * @return   {WritableStream} A writable stream that represents the child's
	 *                            stdin
	 */
	get stdin () {
		return this.process.stdin;
	}

	/**
	 * A sparse array of pipes to child the child process
	 * @property stdio
	 * @return   {Array} [ child.stdin  || null,
	 *                     child.stdout || null,
	 *                     child.stderr || null ]
	 */
	get stdio () {
		return this.process.stdio;
	}

	/**
	 * A readable stream representing the child's stdout
	 * @property stdout
	 * @return   {ReadableStream} A readable stream representing the child's
	 *                            stdout
	 */
	get stdout () {
		return this.process.stdout;
	}
};

Util.inherits(Prock, EventEmitter);

/**
 * Execute command, similar to ChildProcess.exec
 * @method exec
 * @param  {String}   command  The command to execute
 * @param  {Object}   options  Optional. An object containing the child's
 *                             options. See ChildProcess.spawn
 * @param  {Function} callback Optional. Callback to be executed when the
 *                             command exits.
 * @return {Prock}             A new Prock instance
 */
Prock.exec = function prockExec (command/*, options, callback*/) {
	let options = {};
	let callback = () => {};

	if (typeof arguments[1] === "object") {
		options = arguments[1];
	}

	if (arguments[1] instanceof Function) {
		callback = arguments[1];
	} else if (arguments[2] instanceof Function) {
		callback = arguments[2];
	}

	return new Prock(command, options, callback);
};

/**
 * Spawn a command, similiar to ChildProcess.spawn
 * @method spawn
 * @param  {String} command The command to spawn
 * @param  {Array}  args    Optional. An array of arguments to pass to the
 *                          command
 * @param  {Object} options Optional. An object containing the child's
 *                          options. See ChildProcess.spawn
 * @return {Prock}          A new Prock instance
 */
Prock.spawn = function procSpawn (command/*, args, options*/) {
	let args = [];
	let options = {};

	if (Array.isArray(arguments[1])) {
		args = arguments[1];
	}

	if (typeof arguments[1] === "object") {
		options = arguments[1];
	} else if (typeof arguments[2] === "object") {
		options = arguments[2];
	}

	if (args.length) {
		command += " " + args.join(" ");
	}

	return new Prock(command, options);
};

/**
 * Return a new Prock instance
 * @method init
 * @param  {String}   command  The command to run
 * @param  {Object}   options  Optional. Options for child_process.spawn
 * @param  {Function} callback (error, stdout, stderr, code, signal) => { ... }
 * @return {Prock}             The new Prock instance
 */
Prock.init = function prockInit (command, options, callback) {
	return new Prock(command, options, callback);
};

module.exports = Prock;
